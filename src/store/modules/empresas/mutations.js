export default {
  'SET_EMPRESAS' (state, data) {
    state.empresas = data
  },

  'SET_CIDADE' (state, data) {
    state.cidades = data
  },

  'SET_CATEGORIAS' (state, data) {
    state.categorias = data
  }

  
}

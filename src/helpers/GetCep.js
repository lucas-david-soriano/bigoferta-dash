import axios from 'axios'
const getCep = cep => {
    return fetch( `http://viacep.com.br/ws/${cep}/json/` ).then( r => r.json() )
}
export default getCep

import Vue from 'vue'
import Router from 'vue-router'
import Auth from '../helpers/Auth'
import VerifyToken from '../helpers/VerifyToken'
import Login from '../components/Login'
import Home from '../components/Home'

import ChatRoot from '../components/user/ChatRoot'
import Chat from '../components/user/chat/Index'

import MeusDados from '../components/user/MeusDados'
import Ofertas from '../components/user/Ofertas'
import Indicadores from '../components/user/Indicadores'

import Pedidos from '../components/user/pedidos/Pedidos'
import PedidosAtivos from '../components/user/pedidos/PedidosAtivos'
import PedidosArquivados from '../components/user/pedidos/PedidosArquivados'

import Produtos from '../components/user/produtos/Produtos'

import Empresas from '../components/user/empresas/Index'

import Error404 from '../components/Error404'
Vue.use(Router)

export default new Router({
  mode:"history",
  routes: [
    {
        path: '/',
        redirect:"/login"
    },
    {
        path: '/login',
        name: 'login',
        component: Login,
        //beforeEnter:VerifyToken
    },
    {
        path: '/home',
        name: 'home',
        component: Home,
        beforeEnter:Auth
    },
    {
      path: '/empresas',
      name: 'empresas',
      component: Empresas,
      beforeEnter:Auth
  },
    {
        path: '/produtos',
        name: 'produtos',
        component: Produtos,
        beforeEnter:Auth
    },
    {
        path: '/pedidos',
        name: 'pedidos',
        component: Pedidos,
        redirect:'/pedidosativos',
        //beforeEnter:Auth,
        children:[
            {
                path: '/pedidosativos',
                name: 'pedidosativos',
                component: PedidosAtivos,
                //beforeEnter:Auth
            },
            {
                path: '/pedidosarquivados',
                name: 'pedidosarquivados',
                component: PedidosArquivados,
                //beforeEnter:Auth
            }
        ]
    },
    {
        path: '/ofertas',
        name: 'ofertas',
        component: Ofertas,
        //beforeEnter:Auth
    },
    {
        path: '/chat',
        name: 'chatroot',
        component: ChatRoot,
        //beforeEnter:Auth,
        children:[
            {
                path: '/chat/:id',
                name: 'chat',
                component: Chat,
                //beforeEnter:Auth
            }
        ]
    },
    {
        path: '/indicadores',
        name: 'indicadores',
        component: Indicadores,
        //beforeEnter:Auth
    },
    {
        path: '/meusdados',
        name: 'meusdados',
        component: MeusDados,
        //beforeEnter:Auth
    },
    {
        path: '*',
        name: '404',
        component: Error404
    }
  ]
})

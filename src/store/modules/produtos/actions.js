export default {
  loadProdutos ({getters,commit}, empresa)
  {
    return new Promise( (res,rej) => {
      getters.getApi.get(`produtosPorEmpresa/${empresa._id}`).then( resp => {
        commit('SET_PRODUTOS', resp.data.data )
        res(resp.data.status)
      })
    })
  },

  saveProdutos ({getters,commit}, produto)
  {
    /*for(var pair of produto.entries()) {
      console.log(pair[0]+ ', '+ pair[1]);
    }*/

    return new Promise( (res,rej) => {
      getters.getApi.post(`produto`, produto).then( resp => {
        res(resp.data)
      })
    })
  },

  editaProduto ({getters,commit}, produto) {
    return new Promise( (res,rej) => {
      getters.getApi.put(`produto`, produto).then( resp => {
        res(resp.data)
      }).catch(e => {
        rej(e)
      })
    })
  },

  listaCategoriaProduto ({getters, commit}) 
  {
    return new Promise( (res, rej) => {
      getters.getApi.get(`categoriaproduto`, ).then( resp => {
        commit('SET_CATEGORIAPRODUTO', resp.data.data)
        res(resp.data)
      })
    })
  },
}

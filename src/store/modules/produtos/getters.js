export default {
  getProdutos: state => state.produtos,
  getCategoriasProdutos: state => state.categorias
}

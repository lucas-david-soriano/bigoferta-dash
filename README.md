# Sistema Big Oferta

```
Sistema para gestão de marketing
```


## Principais Tecnologias:

Descrição | Versão
--------- | ------:
[axios](https://github.com/axios/axios) | "^0.18.0"
[vue](https://vuejs.org/) | "^2.5.2"
[vue-router](https://router.vuejs.org/en/) | "^3.0.1"
[vuex](https://vuex.vuejs.org/en/intro.html)| "^3.0.1"
[stylus](http://stylus-lang.com/) | "^0.54.5"
[babel-loader](https://babeljs.io/) | "^7.1.1"
[vuetify](https://vuetifyjs.com/) | "^1.1.10"
[momentjs](https://momentjs.com/) | "^2.22.2"
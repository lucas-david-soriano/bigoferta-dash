
export default {
  cadastroEmpresas ({getters, commit}, data) {
    return new Promise( (res,rej) => {
      getters.getApi.post(`empresa`, data).then( resp => {
        res(resp.data)
      })
    })
  },

  alteraEmpresa({commit, getters, rootState}, data)
	{
    return new Promise( (res,rej) => {
        getters.getApi.put(`empresa`, data).then( resp => {
          res(resp.data)
        })
    })
  },

  deletaEmpresa({commit, getters, rootState}, data)
	{       
    return new Promise( (res,rej) => {
        getters.getApi.delete(`empresa/${data}`).then( resp => {
          res(resp.data)
        })
    })
  },

  listaCidade({commit, getters})
	{
      return new Promise( (res,rej) => {
        getters.getApi.get(`cidade`).then( resp => {
          commit('SET_CIDADE', resp.data.data)
          res(resp.data)
        })
      })
  },

  listaCategoria ({getters, commit}, data) {

      return new Promise( (res,rej) => {
        getters.getApi.get(`categoriaempresa`, data).then( resp => {
          commit('SET_CATEGORIAS', resp.data.data)
          res(resp.data)
        })
      })
  },
}

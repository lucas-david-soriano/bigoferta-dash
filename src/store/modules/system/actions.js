import axios from 'axios'

export default {
	loggout(context)
	{
    sessionStorage.removeItem("token")
  },
  setToken ({commit,getters}, data) {
    commit('SET_TOKEN', data.token)

    //commit('SET_EMPRESAS', data.user.estabelecimentos)
    commit('SET_USER', data.user)
    //data.user.estabelecimentos.length > 0 ? commit('SELECT_EMPRESA', data.user.estabelecimentos[0]) : commit('SELECT_EMPRESA', {nome:'Nenhuma empresa selecionada'})
  },

  setEmpresa (context, emp) {
    context.commit('SELECT_EMPRESA', emp)
  },

  checkUsuario ({getters, commit}) {
    getters.getApi.get(`usuario/check` ).then( resp => {
      let dataUser = resp.data.data
      commit('SET_DATAUSER', dataUser )
      commit('SET_ISLOADING_CHECK_USER', false)
      if (dataUser.empresas.length > 0){
        let empresaSelecionada = JSON.parse(sessionStorage.getItem('empresaSelecionada'))
        if(!empresaSelecionada){
          console.log('Pode setar empresa')
          commit('SELECT_EMPRESA', dataUser.empresas[0])
        }
      }else{
        console.log('Não tem empresa')
        sessionStorage.removeItem('empresaSelecionada')
        commit('SELECT_EMPRESA', false)
      }
      //
		})
  }
}

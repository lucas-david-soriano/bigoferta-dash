import system from './modules/system/index.js'
import produtos from './modules/produtos/index.js'
import ofertas from './modules/ofertas/index.js'
import pedidos from './modules/pedidos/index.js'
import chat from './modules/chat/index.js'
import root from './modules/root/index.js'
import empresas from './modules/empresas/index.js'
import categoriaEmpresa from './modules/categoriaEmpresa/index.js'
import categoriaProduto from './modules/categoriaProduto/index.js'
export default {
	modules: {
		system, produtos, ofertas, pedidos, chat, root, empresas, categoriaEmpresa, categoriaProduto
	}
}

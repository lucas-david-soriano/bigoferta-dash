export default {
  getEmpresas: state => state.empresas,
  getCidades: state => state.cidades,
  getCategorias: state => state.categorias,
}

import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import axios from 'axios'
import Vuex from 'vuex'
import router from './router'
import VueChartkick from 'vue-chartkick'
import Chart from 'chart.js'
import EventHub from 'vue-event-hub'

Vue.use(EventHub)
Vue.use(VueChartkick, {adapter: Chart})
Vue.use(Vuex)
Vue.use(Vuetify)

import 'vuetify/dist/vuetify.min.css'

import VuexStore from './store'
const store = new Vuex.Store(VuexStore)
let {state} = VuexStore.modules.system

state.$api = axios.create({
  //baseURL: 'https://api.bigoferta.com/'
  baseURL: 'http://6d041d28.ngrok.io/'
})


//config Authorization
state.$api.interceptors.request.use( config => {
    config.headers.common['Authorization'] = state.token
    return config
}, error => {
    return Promise.reject(error)
})

/*
//config Authorization
state.$api.interceptors.response.use( resp => {
    return resp
}, error => {
    if( error.response.status == 403 )
        sessionStorage.removeItem("token")
    return Promise.reject(error)
})
*/

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})

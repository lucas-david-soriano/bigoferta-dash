export default {
  'SET_PRODUTOS' (state, p) {
    state.produtos = p
  },

  'SET_CATEGORIAPRODUTO' (state, categoria) {
    state.categorias = categoria
  }
}

export default {
  loadCategoriasProduto({commit, getters}) {
    return new Promise( (res,rej) => {
      getters.getApi.get(`categoriaproduto`).then( resp => {
        commit('SET_CATEGORIAS_PRODUTOS', resp.data.data )
        res(resp.data.status)
      })
    })
  }
}
